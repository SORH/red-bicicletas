const nodemailer = require('nodemailer');
const sgTransport = require('nodemailer-sendgrid-transport');
const sgMail = require('@sendgrid/mail');

let mailConfig;

if (process.env.NODE_ENV === 'production'){
  console.log('Esta en produccion');
  const options = {
    auth: {
      api_key: process.env.SENDGRID_API_SECRET
    }
  }
  mailConfig = sgTransport(options);
  module.exports = process.env.SENDGRID_API_SECRET
} else {
  if (process.env.NODE_ENV === 'staging'){
    console.log('XXXXXXXXXXXXXXXXX');
    const options = {
      auth: {
        api_key: process.env.SENDGRID_API_SECRET
      }
    }
    mailConfig = sgTransport(options);
  } else {
    //all emails are catched by ethereal.email
    mailConfig ={
      host: 'smtp.ethereal.email',
      port: 587,
      auth: {
        user: process.env.ethereal_user,
        pass: process.env.ethereal_pwd
      }
    };
  }
}
/*
  user: 'teresa60@ethereal.email',
  pass: 'sEbQ7SrTW6C4mfhS1K'
*/

module.exports = nodemailer.createTransport(mailConfig);
