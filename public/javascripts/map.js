var mymap = L.map('main_map').setView([51.505, -0.09], 13);

L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {

  attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',

}).addTo(mymap);

//L.marker([51.5, -0.09]).addTo(mymap);
//L.marker([51.49, -0.09]).addTo(mymap);
//L.marker([51.51, -0.09]).addTo(mymap);

$.ajax({
  dataType: "json",
  url: "api/bicicletas",
  success: function(result){
    console.log(result);
    result.bicicletas.forEach(function(bici){
      L.marker(bici.ubicacion,{title: bici.id}).addTo(mymap);
    });
  }
})
