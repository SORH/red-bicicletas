var mongoose = require('mongoose');
var Reserva = require('./reserva');
var Schema = mongoose.Schema;
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const uniqueValidator = require('mongoose-unique-validator');

const saltRounds = 10; //Es para la aleatoriedad

const Token = require('../models/token');
const mailer = require('../mailer/mailer');

const validateEmail = function(email) {
  const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;//Expresiones Regex
  return re.test(email);
}

if (process.env.NODE_ENV === 'development'){
  var direc = "http://localhost:3000";
} else {
  var direc = "https://red-bicicletas1.herokuapp.com";
};

var usuarioSchema = new Schema({
    //Para la parte de seguridad
    nombre: {
      type: String,
      trim: true,
      required: [true, 'El nombre es obligatorio']
    },
    email: {
      type: String,
      trim: true,
      required: [true, 'El email es obligatorio'],
      lowercase: true,
      unique: true,//Para que las contraseñas sean unicas
      validate: [validateEmail, 'Por favor, ingrese un email valido'],
      match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
    },
    password: {
      type: String,
      required: [true, 'El password es obligatorio']
    },

    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
      type: Boolean,
      default: false,
    },

});

usuarioSchema.plugin(uniqueValidator,{message: 'El {PATH} ya existe con otro usuario.'});

usuarioSchema.pre('save',function(next){
  if (this.isModified('password')){
    this.password = bcrypt.hashSync(this.password,saltRounds);
  }
  next();
});

usuarioSchema.methods.validPassword = function(password){
  return bcrypt.compareSync(password,this.password);
};

usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb){
    var reserva = new Reserva({usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta});
    console.log(reserva);
    reserva.save(cb);
};

usuarioSchema.methods.enviar_email_bienvenida = function(cb){
  const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
  const email_destination = this.email;
  token.save(function (err){
    if (err) {return console.log(err.message);}

    const mailOptions = {
      from: 'santiagooroz15@gmail.com',
      to: email_destination,
      subject: 'Verificación de cuenta',
      text: 'Hola,\n\n' + 'Por favor, para verificar su cuenta haga click en este link: \n' + direc + '\/token/confirmation\/' + token.token
    };

    mailer.sendMail(mailOptions,function(err){
      if (err) {return console.log('Que esta pasando' + err.message);}
      console.log('A verification email has been sent to: ' + email_destination + '.');
    });
  });
};

usuarioSchema.methods.resetPassword = function(cb) {
  const token = new Token({_userId: this.id,token: crypto.randomBytes(16).toString('hex')});
  const email_destination = this.email;
  token.save(function(err){
    if (err) {return cb(err);}

    const mailOptions = {
      from: 'santiagooroz15@gmail.com',
      to: email_destination,
      subject: 'Reseteo de password de cuenta',
      text: 'Hola,\n\n' + 'Por favor, para resetear el password de su cuenta haga click en este link: \n' + direc + '\/resetPassword\/' + token.token
    };

    mailer.sendMail(mailOptions,function(err){
      if (err) {return cb(err);}
      console.log('Se envio un email para resetear el password a: ' + email_destination + '.');
    });
    cb(null);
  });
};

usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition,callback){
  const self = this;
  console.log(condition);
  self.findOne({
    $or:[
      {'googleId':condition.id},{'email':condition.emails[0].value}
    ]}, (err,result) => {
      if (result) {
        callback(err,result)
      } else {
        console.log('---------- CONDITION ----------');
        console.log(condition);
        let values = {};
        values.googleId = condition.id;
        values.email = condition.emails[0].value;
        values.nombre = condition.displayName || 'SIN NOMBRE';
        values.verificado = true;
        values.password = crypto.randomBytes(16).toString('hex');
        console.log('---------- VALUES ----------');
        console.log(values);
        self.create(values,(err,result) => {
          if (err) {console.log(err);}
          return callback(err,result)
        })
      }
    }
  )
};

usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(condition,callback){
  const self = this;
  console.log(condition);
  self.findOne({
    $or:[
      {'facebookId':condition.id},{'email':condition.emails[0].value}
    ]}, (err,result) => {
      if (result) {
        callback(err,result)
      } else {
        console.log('---------- CONDITION ----------');
        console.log(condition);
        let values = {};
        values.facebookId = condition.id;
        values.email = condition.emails[0].value;
        values.nombre = condition.displayName || 'SIN NOMBRE';
        values.verificado = true;
        values.password = crypto.randomBytes(16).toString('hex');
        console.log('---------- VALUES ----------');
        console.log(values);
        self.create(values,(err,result) => {
          if (err) {console.log(err);}
          return callback(err,result)
        })
      }
    }
  )
};


module.exports = mongoose.model('Usuario', usuarioSchema);
