var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');
var mongoose = require('mongoose');

var base_url = "http://localhost:3000/api/bicicletas";

describe('Bicicleta API', () => {

    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB,{useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false});
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('We are connected to test database!');
            done();
        });

    });

   afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if(err) console.log(err);
            done();
        });
    });

    describe('GET BICICLETAS /', () => {
        it('Status 200', (done) => {
            request.get(base_url, function(error, response, body){
                if(error) console.log(error);
                var result = JSON.parse(body);
                Bicicleta.allBicis(function(err, bicis){
                    expect(response.statusCode).toBe(200);
                    expect(bicis.length).toEqual(0);
                    done();
                });
            });
        });
    });

    describe('POST BICICLETAS /create', () =>{
        it('Status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{ "code": 10, "color": "Rojo", "modelo": "Urbana", "lat": -33, "lng": -70}';
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: aBici,
            },
            function(error, response, body) {
                if(error) console.log(error);
                var bici = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(bici.Bicicleta.color).toBe("Rojo");
                expect(bici.Bicicleta.ubicacion[0]).toBe(-33);
                expect(bici.Bicicleta.ubicacion[1]).toBe(-70);
                done();
            });
        });
    });

    describe('POST BICICLETAS /update', () => {
        it('STATUS 200', (done) => {
            var aBici = new Bicicleta({code: 5, color: "Negro", modelo: "Urbana"});
            Bicicleta.add(aBici, function(err, newBici){
                if(err) console.log(err);
            });

            var headers = {'content-type' : 'application/json'};
            var biciB = '{ "code": 5, "color": "Blanco", "modelo": "Urbana", "lat": -33, "lng": -70}';
            request.post({
                    headers: headers,
                    url: base_url + '/update',
                    body: biciB,
            },
            function(error, response, body){
                if(error) console.log(error);
                var bici = JSON.parse(body);
                Bicicleta.findByCode(5, function(error, targetBici){
                    if(error) console.log(error);
                    expect(response.statusCode).toBe(200);
                    expect(targetBici.color).toBe("Blanco");
                    done();
                });
            });
        });
    });


    describe('DELETE BICICLETAS /delete', () => {
        it('STATUS 204', (done) => {
            var a = new Bicicleta({code: 5, color: "Negro", modelo: "Montaña"});
            Bicicleta.add(a);

            var b = new Bicicleta({code: 3, color: "Amarillo", modelo: "Urbana"});
            Bicicleta.add(b);

            var headers = {'content-type' : 'application/json'};
            var aBici = '{"code": 5}';
            request.delete({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/delete',
                body: aBici
                },
                function(error, response, body){
                    expect(response.statusCode).toBe(204);
                    expect(Bicicleta.allBicis.length).toBe(1);
                    done();
                }
            );
        });
    });

});

/*
beforeEach(() => {
  Bicicleta.allBicis = [];
});

describe('Bicicleta API', () => {
  describe('GET BICICLETAS /', () => {
    it('Status 200', () => {
      expect(Bicicleta.allBicis.length).toBe(0);

      var a = new Bicicleta(1,'rojo','urbana',[51.5, -0.092]);
      Bicicleta.add(a);

      request.get('http://localhost:3000/api/bicicletas', function(error,response,body){
        expect(response.statusCode).toBe(200);
      });
    });
  });

  describe('POST BICICLETAS /create', () => {
    it('Status 200', (done) => {
      var headers = {'content-type':'application/json'};
      var aBici = '{"id":10,"color":"rojo","modelo":"urbana","lat":51.5,"lng":-0.092}';
      request.post({
          headers: headers,
          url: 'http://localhost:3000/api/bicicletas/create',
          body: aBici
        }, function(error,response,body) {
            expect(response.statusCode).toBe(200);
            expect(Bicicleta.findById(10).color).toBe("rojo");
            done();//Cuando se pone done al principio, se debe de poner done al final para que acabe el test
      });
    });
  });

  describe('POST UPDATE /update', () => {
    it('Status 200', (done) => {
      var a = new Bicicleta(10,'rojo','urbana',[51.5,-0.092]);
      Bicicleta.add(a);
      var headers = {'content-type':'application/json'};
      var aBici = '{"id":10,"color":"verde","modelo":"montaña","lat":51.49,"lng":-0.091}'
      request.post({
          headers: headers,
          url: 'http://localhost:3000/api/bicicletas/update',
          body: aBici
        }, function(error,response,body) {
            expect(response.statusCode).toBe(200);
            expect(Bicicleta.findById(10).color).toBe("verde");
            expect(Bicicleta.findById(10).modelo).toBe("montaña");
            expect(Bicicleta.findById(10).ubicacion).toEqual([51.49,-0.091]);
            done();//Cuando se pone done al principio, se debe de poner done al final para que acabe el test
      });
    });
  });

  describe('DELETE BICICLETAS /delete', () => {
    it('Status 204', (done) => {
      var a = new Bicicleta(10,'rojo','urbana',[51.5,-0.092]);
      Bicicleta.add(a);
      var headers = {'content-type':'application/json'};
      var aBici = '{"id":10,"color":"verde","modelo":"montaña","lat":51.49,"lng":-0.091}'
      request.delete({
          headers: headers,
          url: 'http://localhost:3000/api/bicicletas/delete',
          body: aBici
        }, function(error,response,body) {
            expect(response.statusCode).toBe(204);
            expect(Bicicleta.allBicis.length).toBe(0);
            done();//Cuando se pone done al principio, se debe de poner done al final para que acabe el test
      });
    });
  });

});
*/
