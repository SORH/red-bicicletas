var Reserva = require('../../models/reserva');
const Usuario = require('../../models/usuario');
var mongoose = require('mongoose');
const Bicicleta = require('../../models/bicicleta');

describe('Testing Usuarios', function(){
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false});
        mongoose.set('useFindAndModify', false);
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('We are connected to test database!')
            done();
        });
    });

    //Se hace un borrador en cascada
    afterEach(function(done){
        Reserva.deleteMany({}, function(err, success){
            if(err) console.log(err);
            Usuario.deleteMany({}, function(err, success){
                if(err) console.log(err);
                Bicicleta.deleteMany({}, function(err, success){
                    if(err) console.log(err);
                    done();
                });
            });
        });
    });


    describe('Cuando un Usuario reserva una bici', () => {
        it('Debe existir la reserva', (done) => {
            const usuario = new Usuario({nombre: 'Santiago'});
            usuario.save();
            const bicicleta = new Bicicleta({code: 1, color: "Verde", modelo: "Urbana"});
            bicicleta.save();

            var hoy = new Date();
            var manana = new Date();
            manana.setDate(hoy.getDate()+1);
            usuario.reservar(bicicleta.id, hoy, manana, function(err, reserva){
                Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas){
                    console.log(reservas[0]);
                    expect(reservas.length).toBe(1);
                    expect(reservas[0].diasDeReserva()).toBe(2);
                    expect(reservas[0].bicicleta.code).toBe(1);
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                    done();
                });
            });
        });
    });



});
