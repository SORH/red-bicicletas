//Se agrega mongoose
var mongoose = require('mongoose');

var Bicicleta  = require('../../models/bicicleta');

describe('Testing Bicicletas', function(){
  beforeEach(function(done){//Esta parte es para conectar a la base de datos
    var mongoDB = 'mongodb://localhost/testdb';
    mongoose.connect(mongoDB,{useCreateIndex: true,useNewUrlParser:true,useUnifiedTopology: true});

    const db = mongoose.connection;
    db.on('error',console.error.bind(console,'connection error'));
    db.once('open',function(){
      console.log('We are connected to test database!');
      done();
    });
  });

  afterEach(function(done){
    Bicicleta.deleteMany({},function(err,success){
      if (err) console.log(err);
      done();
    });
  });

  describe('Bicicleta.createInstance', () => {
    it('Crea una instancia de Bicicleta', () => {
      var bici = Bicicleta.createInstance(1,'rojo','urbana',[51.5, -0.092]);

      expect(bici.code).toBe(1);
      expect(bici.color).toBe('rojo');
      expect(bici.modelo).toBe('urbana');
      expect(bici.ubicacion).toEqual([51.5,-0.092]);
    });
  });

  describe('Bicicleta.allBicis', () => {
    it('Comienza vacia', (done) => {
      Bicicleta.allBicis(function(err,bicis){
        expect(bicis.length).toBe(0);
        done();
      });
    });
  });

  describe('Bicicleta.add', () => {
    it('Agrega solo una bici', (done) => {
      var aBici = new Bicicleta({code:1,color:"verde",modelo:"urbana"});
      Bicicleta.add(aBici,function(err,newBici){
        if (err) console.log(err);
        Bicicleta.allBicis(function(err,bicis){
          expect(bicis.length).toEqual(1);
          expect(bicis[0].code).toEqual(aBici.code);
          done();
        });
      });
    });
  });

  describe('Bicicleta.findByCode', () => {
    it('Debe devolver la bici con code 1', (done) => {
      Bicicleta.allBicis(function(err,bicis) {
        expect(bicis.length).toBe(0);
        var aBici = new Bicicleta({code:1,color:"verde",modelo:"urbana"});
        Bicicleta.add(aBici,function(err,newBici){
          if (err) console.log(err);
          var aBici2 = new Bicicleta({code:2,color:"roja",modelo:"urbana"});
          Bicicleta.add(aBici2,function(err,newBici){
            if (err) console.log(err);
            Bicicleta.findByCode(1,function(error,targetBici){
              expect(targetBici.code).toBe(aBici.code);
              expect(targetBici.color).toBe(aBici.color);
              expect(targetBici.modelo).toBe(aBici.modelo);
              done();
            });
          });
        });
      });
    });
  });

  describe('Bicicleta.removeByCode', () => {
    it('Remover la bici con code 1', (done) => {
      Bicicleta.allBicis(function(err,bicis) {
        expect(bicis.length).toBe(0);
        var aBici = new Bicicleta({code:1,color:"verde",modelo:"urbana"});
        Bicicleta.add(aBici,function(err,newBici){
          if (err) console.log(err);
          var aBici2 = new Bicicleta({code:2,color:"roja",modelo:"urbana"});
          Bicicleta.add(aBici2,function(err,newBici){
            if (err) console.log(err);
            Bicicleta.removeByCode(1,function(error,targetBici){
              Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(1);
                done();
              });
            });
          });
        });
      });
    });
  });

});

/*
beforeEach(() => {
  Bicicleta.allBicis = [];
});

//Test allBicis
describe('Bicicleta.allBicis', () => {
  it('comienza vacia', () =>{
    expect(Bicicleta.allBicis.length).toBe(0);
  });
});

//Test add
describe('Bicicleta.add', () => {
  it('agregamos una', () =>{
    //Bicicleta.allBicis = [];
    expect(Bicicleta.allBicis.length).toBe(0);

    var a = new Bicicleta(1,'rojo','urbana',[51.5, -0.092]);
    Bicicleta.add(a);

    expect(Bicicleta.allBicis.length).toBe(1);
    expect(Bicicleta.allBicis[0]).toBe(a);

  });
});

//Test findById
describe('Bicicleta.findById', () => {
  it('debe devolver la bici con id 1', () =>{
    //Bicicleta.allBicis = [];
    expect(Bicicleta.allBicis.length).toBe(0);

    var aBici = new Bicicleta(1,'verde','urbana');
    var aBici2 = new Bicicleta(2,'rojo','montaña');
    Bicicleta.add(aBici);
    Bicicleta.add(aBici2);

    var targetBici = Bicicleta.findById(1);
    expect(targetBici.id).toBe(1);
    expect(targetBici.color).toBe(aBici.color);
    expect(targetBici.modelo).toBe(aBici.modelo);

  });
});

//Test removeById
describe('Bicicleta.removeById', () => {
  it('agregamos una', () =>{
    //Bicicleta.allBicis = [];
    expect(Bicicleta.allBicis.length).toBe(0);

    var a = new Bicicleta(1,'rojo','urbana',[51.5, -0.092]);
    Bicicleta.add(a);

    expect(Bicicleta.allBicis.length).toBe(1);
    expect(Bicicleta.allBicis[0]).toBe(a);

    Bicicleta.removeById(1);
    expect(Bicicleta.allBicis.length).toBe(0);

  });
});
*/
