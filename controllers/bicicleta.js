var Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = function(req,res){
  Bicicleta.allBicis(function(err,bicis){
    res.render('bicicletas/index',{bicis:bicis});
  });
}

exports.bicicleta_create_get = function(req,res){
  res.render('bicicletas/create');
}

exports.bicicleta_create_post = function(req,res){
  var bici = Bicicleta.createInstance(req.body.code, req.body.color, req.body.modelo);
  bici.ubicacion = [req.body.lat, req.body.lng];
  Bicicleta.add(bici);

  res.redirect('/bicicletas');
}

exports.bicicleta_update_get = function(req,res){
  Bicicleta.findByCode(req.body.code, function(error, bici){
        if(error) console.log(error);
        res.render('bicicletas/update', {bici});
  });
}

exports.bicicleta_update_post = function(req,res){
  Bicicleta.findOneAndUpdate(
    {"code": req.body.code},
    {
      "code": req.body.code,
      "color": req.body.color,
      "modelo": req.body.modelo,
      "ubicacion": [req.body.lat, req.body.lng]
    },
      function(error, biciUpdate){
        res.status(200);
        res.redirect('/bicicletas');
      }
    );
}

exports.bicicleta_delete_post = function(req,res){
  Bicicleta.removeByCode(
       req.body.code,
       function(error, biciDelete){
           res.status(204);
           res.redirect('/bicicletas');
       }
   );
}
