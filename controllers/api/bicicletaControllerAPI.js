var Bicicleta = require('../../models/bicicleta');//Los dos puntos son para salir de la carpeta

exports.bicicleta_list = function(req, res){
    Bicicleta.allBicis(function(err, bicis){
        res.status(200).json({
            bicicletas: bicis
        });
    });
}

exports.bicicleta_create = function(req, res){
    var bici = Bicicleta.createInstance(req.body.code, req.body.color, req.body.modelo)
    bici.ubicacion = [req.body.lat, req.body.lng];

    Bicicleta.add(bici);
    res.status(200).json({
      Bicicleta: bici
    });
}

exports.bicicleta_delete = function(req, res){
    Bicicleta.removeByCode(
        req.body.code,
        function(error, biciDelete){
            res.status(204).send();
        }
    );
}

exports.bicicleta_update = function(req, res){
    Bicicleta.findOneAndUpdate({"code": req.body.code},
        {
            "code": req.body.code,
            "color": req.body.color,
            "modelo": req.body.modelo,
            "ubicacion": [req.body.lat, req.body.lng]
        },
        function(error, biciUpdate){
            res.status(200).json({
                Bicicleta: biciUpdate
            });
        }
    );
}

/*
exports.bicicleta_list = function(req, res){
  res.status(200).json({
    bicicletas: Bicicleta.allBicis
  })
}

exports.bicicleta_create = function(req,res){
  var bici = new Bicicleta(req.body.id,req.body.color,req.body.modelo);
  bici.ubicacion = [req.body.lat,req.body.lng];

  Bicicleta.add(bici);

  res.status(200).json({
    bicicleta: bici
  });
}

exports.bicicleta_update = function(req,res){
  var bici = Bicicleta.findById(req.body.id);
  bici.id = req.body.id;
  bici.color = req.body.color;
  bici.modelo = req.body.modelo;
  bici.ubicacion = [req.body.lat,req.body.lng];
  res.status(200).json({
    bicicleta: bici
  });
}

exports.bicicleta_delete = function(req,res){
  Bicicleta.removeById(req.body.id);
  res.status(204).send();
}
*/
